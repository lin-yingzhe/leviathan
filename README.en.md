# leviathan

#### Description

Database Tools
Based on
- tauri-app(https://github.com/tauri-apps/tauri)
- material-ui(https://github.com/mui-org/material-ui)

#### Software Architecture

CS Desktop App

#### Installation

1.  npm install/yarn install

#### Instructions

- Support SSH Tunnel
  - support password method
  - support privatekey method

- Support List Cassandra Data
  - support single row of data
  - support data that column with udt list type
  - support pagingdata from backend

#### Preview

- Support SSH Tunnel

![输入图片说明](register.png)
![输入图片说明](list.png)

- Support List Cassandra Data
![输入图片说明](leviathan.png)

#### Notice
must install Edge WebView2(https://developer.microsoft.com/en-us/microsoft-edge/webview2/)

#### Contribution

#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
