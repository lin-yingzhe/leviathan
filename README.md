# leviathan

#### 介绍

数据库工具集  
基于：
- tauri-app(https://github.com/tauri-apps/tauri)
- material-ui(https://github.com/mui-org/material-ui)

#### 软件架构

CS架构 桌面端APP

#### 安装教程

1.  npm install/yarn install

#### 使用说明

- 支持SSH通道
  - 支持密码认证
  - 支持PEM文件认证

- 支持罗列Cassandra中数据
  - 支持后台分页和后台过滤
  - 支持单行数据显示
  - 支持列表型UDT列显示（list<frozen<udt>>)

#### 预览图

- SSH通道

![输入图片说明](register.png)
![输入图片说明](list.png)

- 罗列Cassandra中数据
![输入图片说明](leviathan.png)

#### 参与贡献

#### 注意
务必确认已安装Edge WebView2(https://developer.microsoft.com/en-us/microsoft-edge/webview2/)

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
